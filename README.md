# ETH SPH x ACE / ML Workshops

![ml workshop](https://sph.ethz.ch/wp-content/uploads/2019/10/ML-Features-Image-16-9-new.jpg)
***

### Getting Started

First, make sure that you followed the instructions [here](https://drive.google.com/open?id=1lbJXrgPVnfNyNwYby_bJcEYJ3pZi7z2u) for the initial setup and you created the conda environment correctly.

Activate the conda environment:

``` bash
source activate ml-workshop
```
We mainly work with Jupyter Notebook documents. Open Jupyter with:

``` bash
jupyter notebook
``` 
 
You can deactivate the environment when you have finished working on the project:

``` bash
source deactivate ml-workshop
```

 
## Project directory

``` bash
.
├── data               # datasets
├── sessions           # notebooks used during sessions
├── exercises          # hands-on problems on IoT data 
└── environment.yml    # conda env configuration

```